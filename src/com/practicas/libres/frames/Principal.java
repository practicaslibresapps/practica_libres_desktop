package com.practicas.libres.frames;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import com.practicas.libres.listeners.BotonListener;

import javax.swing.UIManager;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField password_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 406, 656);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		this.setUndecorated(true);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bienvenido");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 41));
		lblNewLabel.setBounds(10, 223, 386, 50);
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Principal.class.getResource("/sources/icons/clear_24px.png")));
		btnNewButton.setForeground(new Color(153, 153, 153));
		btnNewButton.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.setBounds(350, 11, 46, 36);
		btnNewButton.setBackground(new Color(0,0,0,0));
		btnNewButton.setBorder(null); 
		btnNewButton.setBorderPainted(false);
		btnNewButton.setContentAreaFilled(false); 
		btnNewButton.setOpaque(false);
		btnNewButton.addActionListener(new BotonListener());
		panel.add(btnNewButton);
		
		PanelImage imagen = new PanelImage();
		imagen.setBounds(102, 40, 200, 200);
		panel.add(imagen);
		imagen.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setBounds(51, 318, 307, 50);
		panel.add(textField);
		textField.setColumns(10);
		
		password_1 = new JPasswordField();
		password_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		password_1.setColumns(10);
		password_1.setBounds(51, 449, 307, 50);
		panel.add(password_1);
	}
}
