package com.practicas.libres.frames;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class PanelImage extends JPanel {

	@Override
	public void paintComponent(Graphics grafico) {
		// TODO Auto-generated method stub
		try {
			ImageIcon imagen = new ImageIcon(ImageIO.read(new File("src/sources/icons/Icon.png")));
			Graphics2D grafico2 = (Graphics2D) grafico;
			grafico2.drawImage(imagen.getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
			System.out.println("aqui");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
